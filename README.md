# ResearchTalks

Research talks I've watched and enjoyed them. A few of them are available on this [YouTube playlist](https://www.youtube.com/playlist?list=PL9qnQiNEYZfFJsEc10JgjSMNtoMINT9Eb).

## Talks

### More technical

* [Adventures with Types in Haskell (1-4) by Simon Peyton Jones](https://www.youtube.com/watch?v=6COvD8oynmI)
  * [Lecture 1](https://www.youtube.com/watch?v=6COvD8oynmI)
* [Bits of Advice For the VM Writer by Cliff Click](https://www.youtube.com/watch?v=Hqw57GJSrac&list=PL9qnQiNEYZfFJsEc10JgjSMNtoMINT9Eb&index=12&t=0s) @ VMSS16
* [Collapsing Towers of Interpreters by Tiark Rompf](https://www.youtube.com/watch?v=91m8X8hSEXg) @ POPL2018
* [Compiling the Web – Building a Just-in-Time Compiler for JavaScript by Andreas Gal](https://www.microsoft.com/en-us/research/video/compiling-the-web-building-a-just-in-time-compiler-for-javascript/) @ MSResearch 2008
* [Decoupling Algorithms from the Organization of Computation for High-Performance Graphics & Imaging by Jonathan Ragan-Kelley](https://www.youtube.com/watch?v=dnFccCGvT90) @ MSResearch 2016
* [Let's Take Machine Learning from Alchemy to Electricity by Ali Rahimi](https://www.youtube.com/watch?v=ORHFOnaEzPc) @ NIPS2017
* [On Characterizing the Capacity of Neural Networks using Algebraic Topology by William Guss](https://www.youtube.com/watch?v=QDQ9J5E7Uqk) @ MSResearch 2018
* [Stabilizer: Statistically Sound Performance Evaluation by Emery Berger](https://www.microsoft.com/en-us/research/video/stabilizer-statistically-sound-performance-evaluation/) @MSResearch 2013
* [The Tensor Algebra Compiler by Fredrik Kjolstad](https://www.microsoft.com/en-us/research/video/the-tensor-algebra-compiler/) @MSResearch 2017; [TACO Compiler](http://tensor-compiler.org/)
* [Verifying Spatial Properties of Array Computations by Domonic Orchard](https://www.youtube.com/watch?v=Wi2_1EoBjQE) @ OOPSLA 2017; [CamFort](https://camfort.github.io)
* [Virtual Machine Warmup Blows Hot and Cold by Edd Barrett](https://www.youtube.com/watch?v=LgCHAU8ZB00) @ OOPSLA 2017; [Krun](https://github.com/softdevteam/krun)

### Less technical

* [Category Theory for the Working Hacker by Philip Wadler](https://www.youtube.com/watch?v=V10hzjgoklA) @ Lambda World 2016
* ["Propositions as Types" by Philip Wadler](https://www.youtube.com/watch?v=IOiZatlZtGU) @ Strange Loop 2015
* [The Resurgence of Software Performance Engineering by Charles E. Leiserson](https://www.microsoft.com/en-us/research/video/the-resurgence-of-software-performance-engineering/) @MSResearch 2016
* [How to Write a Great Research Paper by Simon Peyton Jones](https://www.youtube.com/watch?v=VK51E3gHENc)

#### Programming

## Sources

### General

* [Institute for Advanced Study](https://video.ias.edu/)
* [Microsoft Research @ YouTube](https://www.youtube.com/user/MicrosoftResearch/videos)

### Conferences

* [POPL2018](https://www.youtube.com/channel/UCTp2XBEhj5rBzc2IfysZwYA/videos)
